//初始化整个游戏的精灵，作为游戏开始的入口
import {ResourcesLoader} from "./js/base/ResourcesLoader.js";
import {Director} from "./js/Director.js";
import {BackGround} from "./js/runtime/Background.js";
import {DataStore} from "./js/base/DataStore.js";
import {Land} from "./js/runtime/Land.js";
import {Birds} from "./js/player/Birds.js";
import {StartButton} from "./js/player/StartButton.js";
import {Score} from "./js/player/Score.js";

export class Main {
    constructor() {
        this.canvas = document.getElementById('game_canvas');
        this.ctx = this.canvas.getContext('2d');
        this.dataStore = DataStore.getInstance();
        this.director = Director.getInstance();  //存储类的实例
        const loader = ResourcesLoader.create();
        loader.onLoaded(map => this.onResourceFirstLoaded(map)); //图片资源加载完成之后再进行初始化渲染
    }

    onResourceFirstLoaded(map) {
        this.dataStore.canvas = this.canvas;
        this.dataStore.ctx = this.ctx;
        this.dataStore.res = map;
        this.init();
    }

    init() {

        this.director.isGameOver = false;
        this.dataStore
            .put('background', BackGround)
            .put('land', Land)
            .put('pencils', [])
            .put('birds', Birds)
            .put('startButton', StartButton)
            .put('score', Score);  //本质是个function的引用
        this.registerEvent();
        //创造铅笔要在游戏逻辑运行之前
        this.director.createPencil();
        this.director.run();//由于上面实例化那改了，这里也要改
    }

    registerEvent() {

        this.canvas.addEventListener('touchstart', e => {
            //屏蔽掉事件冒泡
            e.preventDefault();
            if (this.director.isGameOver) {
            console.log('游戏结束');
            this.init();

            } else {
                this.director.birdsEvent();
            }
        });
    }

}