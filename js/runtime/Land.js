//不断移动的陆地
import {Sprite} from "../base/Sprite.js";
import {Director} from "../Director.js";
import {DataStore} from "../base/DataStore.js";


export class Land extends Sprite{
       constructor(){
        const image = Sprite.getImage('land');
        super(image, 0, 0,
            image.width, image.height,
            DataStore.getInstance().canvas.width, DataStore.getInstance().canvas.height-image.height,
            image.width, image.height);
        //地板的水平移动坐标
        //this.landX = 0;
        //地板的移动速度
        this.landSpeed = Director.getInstance().landSpeed;
    }

    draw(){

        this.x = this.x - this.landSpeed;
        if (-this.x> (this.width)){
            this.x = DataStore.getInstance().canvas.width;
        }
        super.draw(this.img,
            0, 0,
            this.width, this.height,
            this.x, this.y,
            this.width, this.height)
        /*
        this.landX = this.landX - this.landSpeed;
        //447是为了留一点距离方便提供一种新玩法（从地下通过）
        //447是this.img.width+150-DataStore.getInstance().canvas.width
        if (-this.landX > (2*this.img.width)){
            this.landX = 0;
        }


        super.draw(this.img,
            0, 0,
            this.width, this.height,
            this.landX,
            this.y,
            this.width,
            this.height)

           */
    }
}