//小鸟类
//是循环地渲染三只小鸟
import {Sprite} from "../base/Sprite.js";
import {DataStore} from "../base/DataStore.js";


export  class Birds extends Sprite{
    constructor() {
        const image = Sprite.getImage('birds');
        super(image, 0, 0, image.width, image.height,
            0, 0, image.width, image.height);

        //小鸟的三种状态需要一个数组去存储
        //小鸟的宽是34，上下宽距是10，小鸟的左右边距是9

        this.clippingX = [21.6,
            21.6 + 81.6 + 43.2,
            21.6 + 81.6 + 43.2 + 81.6 + 43.2];
        this.clippingY = [24, 24, 24];
        this.clippingWidth = [81.6, 81.6, 81.6];
        this.clippingHeight = [57.6, 57.6, 57.6];
        const birdX = DataStore.getInstance().canvas.width / 4;
        this.birdsX = [birdX, birdX, birdX];
        const birdY = DataStore.getInstance().canvas.height / 2;//不需要对外公开的变量可以直接声明
        this.birdsY = [birdY, birdY, birdY];
        const birdWidth = 68;
        this.birdsWidth = [birdWidth, birdWidth, birdWidth];
        const birdHeight = 68;
        this.birdsHeight = [birdHeight, birdHeight, birdHeight];

        this.y = [birdY, birdY, birdY];//储存点击时刻的y坐标
        this.index = 0;
        this.count = 0;
        this.time = 0;
    }
    draw() {
        //切换三只小鸟速度
        const speed = 0.2;
        this.count = this.count + speed;
        if (this.count >= 2) {
            this.count = 0;
        }
        this.index = Math.floor(this.count);//减速器的作用（index碰到小数会显示不出来）

        //模拟重力加速度
        const g = -0.8;

        //向上移动一丢丢偏移量
        const offsetUp = 80;

        //小鸟的位移
        const offsetY  = (g * this.time * (this.time-offsetUp))/2 ;
        for (let i = 0; i <= 2; i++){
            this.birdsY[i] = this.y[i] + offsetY;
        }
        this.time++;

        super.draw(
            this.img,
            this.clippingX[this.index], this.clippingY[this.index],
            this.clippingWidth[this.index], this.clippingHeight[this.index],
            this.birdsX[this.index], this.birdsY[this.index],
            this.birdsWidth[this.index], this.birdsHeight[this.index]
        )
    }
}
