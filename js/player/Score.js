//记分器类
import {DataStore} from "../base/DataStore.js";
import {Director} from "../Director.js";

export class Score{
    constructor(){
        this.ctx = DataStore.getInstance().ctx;
        this.scoreNumber = 0;
        //因为canvas变化很快，所以要控制何时能加分
        this.isScore = true;
    }

    draw(){
        this.ctx.font = '100px Arial';
        this.ctx.fillStyle = '#ff2415';
        this.ctx.fillText(
            this.scoreNumber,
            DataStore.getInstance().canvas.width/2,
            DataStore.getInstance().canvas.height/18,
            1000
        );
        this.ctx.fillText(
            Director.getInstance().time,
            DataStore.getInstance().canvas.width/1.5,
            DataStore.getInstance().canvas.height/18,
            1000
        );
    }
}
