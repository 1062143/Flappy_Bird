//控制游戏的逻辑
import {DataStore} from "./base/DataStore.js";
import {UpPencil} from "./runtime/UpPencil.js";
import {DownPencil} from "./runtime/DownPencil.js";

export class Director {

    constructor(){
        this.dataStore = DataStore.getInstance();
        this.landSpeed = 4;
        this.time = 0;
        this.flag = 0;
    }
    static getInstance(){
        if (!Director.instance){
            Director.instance = new Director();
        }
        return Director.instance;
    }

    createPencil(){
        const minTop = DataStore.getInstance().canvas.height/8;
        const maxTop = DataStore.getInstance().canvas.height/2;
        const top = minTop + Math.random()*(maxTop - minTop);
        this.dataStore.get('pencils').push(new UpPencil(top));
        this.dataStore.get('pencils').push(new DownPencil(top));

    }

    birdsEvent(){
        for(let i=0; i<=2; i++){
            this.dataStore.get('birds').y[i] =
                this.dataStore.get('birds').birdsY[i];//这里就体现出必须要先飞一段距离了
            //birdsY是每时每刻小鸟的y坐标，所以先跳一下就可以把这个高度赋给y
        }
        this.dataStore.get('birds').time = 0;
    }

    //判断小鸟是否和铅笔撞击
    static isStrike(bird, pencil){
        let s = false;
        if (bird.top > pencil.bottom||
            bird.bottom < pencil.top||
            bird.right < pencil.left||
            bird.left > pencil.right
        ){
            s =true;
        }
        return !s;
    }



    //判断小鸟是否撞击地板和铅笔
    check() {
        const birds = this.dataStore.get('birds');
        const land = this.dataStore.get('land');
        const pencils = this.dataStore.get('pencils');
        const score = this.dataStore.get('score');

//地板的撞击判断
        /*if (birds.birdsY[0] + birds.birdsHeight[0] >= land.y) {
            this.isGameOver = true;
        }*/


        if (this.dataStore.get('birds').birdsY[0] > this.dataStore.canvas.height){
            this.time++;
            this.flag = 1;
        }

        if (this.flag === 1 && this.dataStore.get('birds').birdsY[0] < this.dataStore.canvas.height) {
            this.time = 0;
            this.flag = 0;

                score.scoreNumber = score.scoreNumber + 10;

        }
        if (this.time > 300 && (this.dataStore.get('birds').birdsY[0] > this.dataStore.canvas.height)){
            console.log('时间到了');
            this.time = 0;
            this.isGameOver = true;
            return;
        }


        const birdsBorder = {
            top: birds.birdsY[0],
            bottom: birds.birdsY[0] + birds.birdsHeight[0],
            left: birds.birdsX[0],
            right: birds.birdsX[0] + birds.birdsWidth[0]
        };

        const landBorder = {
            top: land.y,
            bottom: land.y + land.height,
            left: land.x,
            right: land.x + land.width,
        };

        const length = pencils.length;
        for (let i=0; i<length; i++){
            const pencil = pencils[i];
            const pencilBorder = {
                top: pencil.y,
                bottom: pencil.y + pencil.height,
                left:pencil.x,
                right: pencil.x + pencil.width,
            };
            if (Director.isStrike(birdsBorder, pencilBorder)){
                console.log('撞到水管');
                this.isGameOver = true;
                return;
            }
            if (Director.isStrike(birdsBorder, landBorder)){

                console.log('撞到地板');
                this.isGameOver = true;
                return;
            }
            if (this.dataStore.get('birds').birdsY[0] < -200 ){

                console.log('撞到天空');
                this.isGameOver = true;
                return;
            }
        }

        //加分逻辑
        if (birds.birdsX[0] > pencils[0].x + pencils[0].width &&
            score.isScore && birds.birdsY[0] < pencils[1].y) {

            score.isScore = false;
            score.scoreNumber++;
        }
    }



    //每秒刷新六十次
    run(){
        this.check();
        if (!this.isGameOver){
            this.dataStore.get('background').draw();

            const pencils = this.dataStore.get('pencils');
            if ((pencils[0].x + pencils[0].width) <= 0 && pencils.length === 4){
                pencils.shift();
                pencils.shift();
                this.dataStore.get('score').isScore = true;
            }
            if (pencils[0].x <= (DataStore.getInstance().canvas.width - pencils[0].width)/2 &&
                pencils.length === 2){
                this.createPencil();
            }
            this.dataStore.get('pencils').forEach(function (value){
                value.draw();
            });
            this.dataStore.get('land').draw();
            this.dataStore.get('score').draw();
            this.dataStore.get('birds').draw();

            //按照一定规则每帧执行
            let timer = requestAnimationFrame(() => this.run());
            this.dataStore.put('timer', timer);

        }else{
            console.log('游戏结束');
            this.dataStore.get('startButton').draw();
            cancelAnimationFrame(this.dataStore.get('timer'));
            this.dataStore.destory();
        }

    }

}