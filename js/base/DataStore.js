//变量储存器 方便在不同的类中储存和修改变量
export class DataStore{

    //因为全局只有一个，所以创造一个单例，即一个类只有一个实例对象//
    static getInstance(){
        if(!DataStore.instance){
            DataStore.instance = new DataStore();
        }
        return DataStore.instance;
    }

    constructor(){
        this.map = new Map();
    }

    put(key, value){
        if (typeof value === 'function'){
            value = new value();//为什么传入进来的参数可以是function？（或者说是一个类？）

        }

        this.map.set(key, value);
        return this;
    }

    get(key){
        return this.map.get(key);
    }

    destory(){
        for(let value of this.map.values()){
            value = null;
        }
    }
}