//精灵的基类，负责初始化精灵加载的资源和大小以及位置
import {DataStore} from "./DataStore.js";

export class Sprite {

    constructor(img = null,
                srcX = 0,
                srcY = 0,
                srcW = 0,
                srcH = 0,
                x = 0, y = 0,
                width = 0, height = 0) {
        this.dataStore = DataStore.getInstance();
        this.ctx = this.dataStore.ctx;
        this.img = img;
        this.srcX = srcX;
        this.srcY = srcY;
        this.srcW = srcW;
        this.srcH = srcH;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;

        /*
        img: 传入的Image对象
        srcX: 要剪裁的x坐标
        srcY:
        srcW: 剪裁的宽度
        srcH:
        x, y: 放置的坐标
        width: 要使用的高度
        height:
        */
    }

    static getImage(key){
        return DataStore.getInstance().res.get(key);
    }

    //传入参数，如果没有赋值则用默认值
        draw(img = this.img,
             srcX = this.srcX,
             srcY = this.srcY,
             srcW = this.srcW,
             srcH = this.srcH,
             x = this.x,
             y = this.y,
             width = this.width,
             height = this.height){

            this.ctx.drawImage(
                img,
                srcX,
                srcY,
                srcW,
                srcH,
                x,
                y,
                width,
                height
            );
        }
}